package com.example.mathgame

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.evernote.android.state.State
import com.evernote.android.state.StateSaver


class additions : Fragment() {


    internal lateinit var gameScoreTextView: TextView
    internal lateinit var timeLeftTextView: TextView
    internal lateinit var goButton: Button

    @State
    var score=0

    @State
    var gameStarted = false

    private var initialCountDown: Long = 10000
    private var countDownInterval: Long =100
    private lateinit var countDownTimer: CountDownTimer // para implementar la varibale timer

    @State
    var timeLeft = 10;







    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        StateSaver.restoreInstanceState(this,savedInstanceState)

        gameScoreTextView= view.findViewById(R.id.text_score)
        goButton = view.findViewById(R.id.button_send)
        timeLeftTextView = view.findViewById(R.id.text_time)

        // guardar el estado de nuestras variables y poderlas reutilizar nuestras variables
        /* if (savedInstanceState != null){
             score=savedInstanceState.getInt(SCORE_KEY)
         }*/
        gameScoreTextView.text =getString(R.string.text_score,score)
        timeLeftTextView.text =getString(R.string.text_time, timeLeft)







    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        StateSaver.saveInstanceState(this,outState)
        countDownTimer.cancel()
        // le ponemos un contador para que ya no vuelva a girar
        //outState.putInt(SCORE_KEY,score)
        // le estamos poniendo en el estado, guardandole un entero con la llave score-key y score
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_additions, container, false)
    }



    companion object {
        private val SCORE_KEY="SCORE"

    }

    fun incrementScore(){


        if(!gameStarted){
            countDownTimer.start()

        }

        score ++
        gameScoreTextView.text = getString(R.string.text_score, score)
    }

    /*private fun resetGame(){
        score= 0
        gameScoreTextView.text =getString(R.string.text_score,score)

        timeLeft = 10
        timeLeftTextView.text =getString(R.string.text_time,timeLeft)

        countDownTimer= object :CountDownTimer(initialCountDown,countDownInterval){

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text =getString(R.string.text_time, timeLeft)
            }



        }
    }*/

    private fun startGame(){

        countDownTimer.start()
        gameStarted = true

    }

    /*private fun restoreGame(){
        gameScoreTextView.text =getString(R.string.text_score,score)

        countDownTimer= object :CountDownTimer
            (timeLeft * 1000L,countDownInterval){

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text =getString(R.string.text_time, timeLeft)
            }



        }
        countDownTimer.start()
    }*/


}

