package com.example.mathgame

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.evernote.android.state.State


class home : Fragment() {

    internal lateinit var button_add: Button
    internal lateinit var button_sub: Button
    internal lateinit var button_mult: Button
    internal lateinit var button_div: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        button_add= view.findViewById(R.id.button_add)
        button_add.setOnClickListener { goToAdd () }



    }

    fun goToAdd (){

    val action= homeDirections.actionHome2ToAdditions2()
        view?.findNavController()?.navigate(action)

    }










    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    companion object {

    }
}
